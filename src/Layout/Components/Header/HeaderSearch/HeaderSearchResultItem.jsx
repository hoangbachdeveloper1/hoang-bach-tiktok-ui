import styles from '../Header.module.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);
function HeaderSearchResultItem() {
    return (
        <li className={cx('result__item')}>
            <div className={cx('result__item__container')}>
                <div className={cx('result__item__image')}>
                    <img
                        src="https://p16-sign-sg.tiktokcdn.com/tos-alisg-avt-0068/2817fd99c7ccc4f0537de44683642389~c5_300x300.webp?x-expires=1694077200&x-signature=dNuVYmNNR7vfErF%2B%2BI1%2FzBWRo1w%3D"
                        alt="user_img"
                    />
                </div>
                <div className={cx('result__item__info')}>
                    <h3 className={cx('result__item__slug')}>bachhoang27071998</h3>
                    <h5 className={cx('result__item__name')}>Nguyễn Hoàng Bách</h5>
                </div>
            </div>
        </li>
    );
}

export default HeaderSearchResultItem;
