import styles from '../Header.module.scss';
import classNames from 'classnames/bind';
import HeaderSearchResultItem from './HeaderSearchResultItem';

const cx = classNames.bind(styles);
function HeaderSearchResult() {
    return (
        <div className={cx('header__search__result')} tabIndex="-1">
            <h4 className={cx('result__title')}>Tài khoản</h4>
            <ul className={cx('result__list')}>
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
                <HeaderSearchResultItem />
            </ul>
        </div>
    );
}

export default HeaderSearchResult;
