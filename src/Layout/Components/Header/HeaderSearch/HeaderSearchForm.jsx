import { useState, useEffect } from 'react';
import Tippy from '@tippyjs/react/headless';
import styles from '../Header.module.scss';
import classNames from 'classnames/bind';
import Proper from '@/components/Proper/Proper/';
import HeaderSearchResult from './HeaderSearchResult';

const cx = classNames.bind(styles);

function HeaderSearchForm() {
    const [search, setSearch] = useState('');
    const [hideClear, setHideClear] = useState(false);

    const handleSearch = (e) => {
        setSearch(e.target.value);
    };
    useEffect(() => {
        search === '' ? setHideClear(false) : setHideClear(true);
    }, [search]);

    useEffect(() => {}, []);

    const [searchResult, setSearchResult] = useState([]);

    return (
        <div className={cx('header__search')}>
            <Tippy
                visible={searchResult.length > 0 ? true : false}
                interactive={true}
                render={(attrs) => (
                    <Proper>
                        <HeaderSearchResult />
                    </Proper>
                )}
            >
                <div className={cx('header__search__container')}>
                    <form className={cx('header__search__form')}>
                        <input
                            className={cx('header__search__input')}
                            type="text"
                            name="headerSearch"
                            value={search}
                            placeholder="Tìm kiếm"
                            onChange={handleSearch}
                        />

                        {hideClear === false ? (
                            <></>
                        ) : (
                            <div
                                className={cx('header__search__icon')}
                                onClick={() => {
                                    setSearch('');
                                }}
                            >
                                <i className="fas fa-times-circle"></i>
                            </div>
                        )}

                        <button className={cx('header__search__button')}>
                            <i className="far fa-search"></i>
                        </button>
                    </form>
                </div>
            </Tippy>
        </div>
    );
}

export default HeaderSearchForm;
