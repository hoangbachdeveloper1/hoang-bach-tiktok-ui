import styles from './Header.module.scss';
import classNames from 'classnames/bind';
import images from '@/assets/images';

const cx = classNames.bind(styles);

function HeaderDownload() {
    return (
        <div className={cx('header__download__container')}>
            <div className={cx('header__download__inner')}>
                <img className={cx('header__download__image')} src={images.downloadImage} alt="Download" />
                <h3>Ứng dụng Tiktok cho máy tính</h3>
                <button className={cx('header__download__button', 'btn', 'btn-danger')}>Tải về</button>
                <div className={cx('header__download__text')}>
                    <p>Tay vào đó, tải ứng dụng di động về</p>
                    <i class="far fa-angle-right"></i>
                </div>
            </div>
        </div>
    );
}

export default HeaderDownload;
