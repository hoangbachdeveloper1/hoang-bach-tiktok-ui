import styles from './Header.module.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

function HeaderNotification({ notification }) {
    return (
        <div className={cx('notification')}>
            <div className={cx('notification__wrapper')}>
                <span className={cx('notification__text')}>{notification}</span>
            </div>
        </div>
    );
}

export default HeaderNotification;
