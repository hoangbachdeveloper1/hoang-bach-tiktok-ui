import { useState } from 'react';
import styles from './Header.module.scss';
import classNames from 'classnames/bind';
import images from '@/assets/images';
import HeaderFunction from './HeaderFunction';
import { Link } from 'react-router-dom';

const cx = classNames.bind(styles);
function HeaderListButton() {
    const [login, setLogin] = useState(true);

    const handleLogin = () => {
        setLogin(true);
    };

    return (
        <div className={cx('header__function')}>
            <div className={cx('header__function__upload')}>
                <Link to="/upload" className={cx('upload__btn', 'btn')}>
                    <img src={images.plusIcon} alt="upload" />
                    <span>Tải lên</span>
                </Link>
            </div>
            {login ? (
                <HeaderFunction />
            ) : (
                <div className={cx('header__function__login')}>
                    <button onClick={handleLogin} className={cx('login__btn', 'btn btn-danger')}>
                        <span>Đăng nhập</span>
                    </button>
                </div>
            )}
        </div>
    );
}

export default HeaderListButton;
