import styles from './Header.module.scss';
import classNames from 'classnames/bind';
import images from '@/assets/images';
import HeaderSettingPopupList from './HeaderSettingPopupList';
import HeaderDownload from './HeaderDownload';
import HeaderNotification from './HeaderNotification';
import HeaderDescriptionButton from './HeaderDescriptionButton';

const cx = classNames.bind(styles);

function HeaderFunction() {
    return (
        <div className={cx('header__function__button')}>
            <div className={cx('header__button')}>
                <div className={cx('header__button__list')}>
                    {/* Download Start */}
                    <div className={cx('header__button__download', 'header__download', 'header__button--icon')}>
                        <img src={images.downloadIcon} alt="Download ico" />
                        <HeaderDownload />
                    </div>
                    {/* Download End */}

                    {/* Message Start */}
                    <div className={cx('header__button__message', 'header__button__relative', 'header__button--icon')}>
                        <img src={images.messageIcon} alt="Messgae ico" />
                        <HeaderDescriptionButton title="Tin nhắn" />
                    </div>
                    {/* Message End */}

                    {/* Notification Start */}
                    <div
                        className={cx(
                            'header__button__notification',
                            'header__button__relative',
                            'header__button--icon',
                        )}
                    >
                        <img src={images.notiIcon} alt="Notification ico" />
                        <HeaderNotification notification={6} />
                        <HeaderDescriptionButton title="Hộp thư" />
                    </div>
                    {/* Notification End */}

                    {/* Avata Start */}
                    <div className={cx('header__button__profile', 'header__button--icon')}>
                        <img
                            src={images.avata}
                            style={{
                                width: 32,
                                height: 32,
                                borderRadius: '50%',
                            }}
                            alt="Avata"
                        />
                        <HeaderSettingPopupList />
                    </div>
                    {/* Avata Emnd */}
                </div>
            </div>
        </div>
    );
}

export default HeaderFunction;
