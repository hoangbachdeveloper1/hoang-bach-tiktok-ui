import { Link } from 'react-router-dom';
import styles from './Header.module.scss';
import classNames from 'classnames/bind';
import images from '@/assets/images';

const cx = classNames.bind(styles);
function HeaderLogo() {
    return (
        <div className={cx('header__logo')}>
            <Link to="/">
                <img className={cx('header__logo__image')} src={images.logo} alt="logo" />
            </Link>
        </div>
    );
}

export default HeaderLogo;
