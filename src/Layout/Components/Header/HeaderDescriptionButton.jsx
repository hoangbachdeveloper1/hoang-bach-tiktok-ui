import styles from './Header.module.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

function HeaderDescriptionButton({ title }) {
    return (
        <div className={cx('header__button__description')}>
            <div className={cx('header__description__wraps')}>{title}</div>
        </div>
    );
}

export default HeaderDescriptionButton;
