import { Link } from 'react-router-dom';
import styles from './Header.module.scss';
import classNames from 'classnames/bind';
import { profileList as Icons } from '@/assets/const/profileList';
import { useState } from 'react';

const cx = classNames.bind(styles);

function HeaderSettingPopupList() {
    const [dark, setDark] = useState(false);
    const handleDarkMode = () => {
        setDark(!dark);
        document.body.classList.toggle('dark-mode');
    };
    const handleLogOut = () => {};

    return (
        <div className={cx('header__profile')}>
            <ul className={cx('header__profile__list')}>
                {Icons.map((item, index) => {
                    return (
                        <li key={index}>
                            <Link
                                to={item.link}
                                className={cx('header__profile__item', item.mode && 'header__profile__mode')}
                            >
                                <div className={cx('item__icon')}>{item.icon}</div>
                                <div className={cx('item__text')}>
                                    <p>{item.title}</p>
                                </div>
                                {item.mode ? (
                                    <div
                                        className={cx('item__mode')}
                                        onClick={handleDarkMode}
                                        onCLick={item.logOut ? handleLogOut : null}
                                    >
                                        <div className={cx('item__mode__button', dark ? 'item__mode__on' : '')}>
                                            <span className={cx('item__mode__point')}></span>
                                        </div>
                                    </div>
                                ) : (
                                    <></>
                                )}
                            </Link>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

export default HeaderSettingPopupList;
