import styles from './Header.module.scss';
import classNames from 'classnames/bind';
import HeaderListButton from './HeaderListButton';
import HeaderSearchForm from './HeaderSearch/HeaderSearchForm';
import HeaderLogo from './HeaderLogo';

const cx = classNames.bind(styles);
function Header() {
    return (
        <header className={cx('header')}>
            <div className={cx('header__wrapper')}>
                <div className={cx('header__inner')}>
                    <HeaderLogo />
                    <HeaderSearchForm />
                    <HeaderListButton />
                </div>
            </div>
        </header>
    );
}

export default Header;
