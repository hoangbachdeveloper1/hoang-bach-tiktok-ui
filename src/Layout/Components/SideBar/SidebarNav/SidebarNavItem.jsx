import { Link } from 'react-router-dom';
import styles from '../Sidebar.module.scss';
import classNames from 'classnames/bind';
import { useState } from 'react';

const cx = classNames.bind(styles);

function SidebarNavItem({ id, title, link, icon, onClick, selected }) {
    const [border, setBorder] = useState(false);

    const handleOnClick = (id) => {
        onClick(id);
    };

    return (
        <div
            className={cx('sidebar__nav__item', selected ? 'selected' : '', border ? 'border' : '')}
            onMouseUp={() => {
                setBorder(false);
            }}
            onMouseDown={() => {
                setBorder(true);
            }}
        >
            <Link
                className={cx('sidebar__nav__link')}
                to={link}
                onClick={() => {
                    handleOnClick(id);
                }}
            >
                {icon}
                <span>{title}</span>
            </Link>
        </div>
    );
}

export default SidebarNavItem;
