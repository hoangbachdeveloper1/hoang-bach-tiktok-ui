import styles from '../Sidebar.module.scss';
import classNames from 'classnames/bind';
import navItems from './NavItems';
import SidebarNavItem from './SidebarNavItem';
import { useState } from 'react';

const cx = classNames.bind(styles);

function SidebarNav() {
    const [selected, setSelected] = useState(0);

    const handleSelected = (id) => {
        setSelected(id);
    };

    return (
        <div className={cx('sidebar__nav')}>
            <div className={cx('sidebar__nav__list')}>
                {navItems.map((nav) => {
                    return (
                        <SidebarNavItem
                            selected={nav.id === selected}
                            key={nav.id}
                            id={nav.id}
                            title={nav.title}
                            icon={nav.icon}
                            link={nav.link}
                            onClick={handleSelected}
                        />
                    );
                })}
            </div>
        </div>
    );
}

export default SidebarNav;
