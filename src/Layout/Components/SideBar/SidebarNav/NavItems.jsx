import { homeIcon, followIcon, exploreIcon, liveIcon } from '@/assets/sidebarIcons/sidebarIcons';

const navItems = [
    {
        id: 0,
        title: 'Dành cho bạn',
        link: '/',
        icon: homeIcon,
    },
    {
        id: 1,
        title: 'Đang follow',
        link: '/follow',
        icon: followIcon,
    },
    {
        id: 2,
        title: 'Khám phá',
        link: '/explore',
        icon: exploreIcon,
    },
    {
        id: 3,
        title: 'LIVE',
        link: '/live',
        icon: liveIcon,
    },
];

export default navItems;
