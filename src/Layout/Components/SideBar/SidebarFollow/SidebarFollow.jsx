import styles from '../Sidebar.module.scss';
import classNames from 'classnames/bind';
import followers from './Followers';
import SidebarFollower from './SidebarFollower';

const cx = classNames.bind(styles);

function SidebarFollow() {
    return (
        <div className={cx('siderbar__follow')}>
            <div className={cx('sidebar__follow__container')}>
                <div className={cx('sidebar__follow__title')}>
                    <h4>Các tài khoản đang follow</h4>
                    <div className={cx('sidebar__follow__list')}>
                        <ul>
                            {followers.map((follower) => {
                                return (
                                    <SidebarFollower
                                        key={follower.id}
                                        image={follower.image_path}
                                        name={follower.name}
                                        tick={follower.blueTick}
                                        description={follower.description}
                                        link={follower.link}
                                    />
                                );
                            })}
                        </ul>
                        <div className={cx('sidebar__follow__more')}>
                            <button>Xem thêm</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SidebarFollow;
