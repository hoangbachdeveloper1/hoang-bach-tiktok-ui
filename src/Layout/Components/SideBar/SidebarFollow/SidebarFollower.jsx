import { Link } from 'react-router-dom';
import styles from '../Sidebar.module.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

function SidebarFollower(props) {
    return (
        <li className={cx('sidebar__follow__item')}>
            <Link to={props.link} className={cx('item__wraper')}>
                <div className={cx('item__img')}>
                    <img width={32} height={32} src={props.image} alt={props.name} />
                </div>
                <div className={cx('item__info')}>
                    <h4>{props.name}</h4>
                    <p>{props.description}</p>
                </div>
                {props.tick ? (
                    <div className={cx('item__tick')}>
                        <i class="fas fa-check-circle"></i>
                    </div>
                ) : (
                    ''
                )}
            </Link>
        </li>
    );
}

export default SidebarFollower;
