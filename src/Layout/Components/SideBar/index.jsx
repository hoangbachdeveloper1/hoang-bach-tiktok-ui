import styles from './Sidebar.module.scss';
import classNames from 'classnames/bind';
import SidebarNav from './SidebarNav/SidebarNav';
import SidebarFollow from './SidebarFollow/SidebarFollow';
import SidebarTags from './SidebarTags/SidebarTags';
import { useEffect, useState } from 'react';
const cx = classNames.bind(styles);

function SideBar() {
    const [sreenHeight, setScreenHeight] = useState(0);
    useEffect(() => {
        const h = window.screen.height;
        console.log(h);
        setScreenHeight(h - 60);
    }, []);
    return (
        <aside className={cx('sidebar')} style={{ height: { sreenHeight } }}>
            <div className={cx('sidebar__inner')}>
                {/* SidebarNav start */}
                <SidebarNav />
                {/* SidebarNav End */}

                {/* Sidebar Follow Start */}
                <SidebarFollow />
                {/* Sidebar Follow End */}

                {/* Sidebar Tags start */}
                <SidebarTags />
                {/* Sidebar Tags End */}
            </div>
        </aside>
    );
}

export default SideBar;
