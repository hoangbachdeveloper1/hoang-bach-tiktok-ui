import { Link } from 'react-router-dom';
import styles from '../Sidebar.module.scss';
import classNames from 'classnames/bind';
import tags from '@/assets/sidebarIcons/SidebarTags';

const cx = classNames.bind(styles);

function SidebarTags() {
    return (
        <div className={cx('sidebar__tag')}>
            <div className={cx('sidebar__tag__button')}>
                <a className={cx('sidebar__tag__link')} href="#">
                    <img
                        src="https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/tiktok/webapp/main/webapp-desktop/045b2fc7c278b9a30dd0.png"
                        alt="create Efect"
                    />
                    <div className={cx('button__text')}>
                        <i class="fal fa-home"></i>
                        <h3>Tạo hiệu ứng</h3>
                    </div>
                </a>
            </div>

            <div className={cx('sidebar__tag__list')}>
                {tags.map((tag) => (
                    <Link className={cx('tag__item')} key={tag.id} to={tag.link}>
                        <div>{tag.title}</div>
                    </Link>
                ))}
            </div>
        </div>
    );
}

export default SidebarTags;
