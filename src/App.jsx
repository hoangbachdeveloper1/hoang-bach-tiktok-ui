import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { publicRoutes } from './routes';
import DefaultLayout from './Layout/DefaultLayout';

function App() {
    return (
        <Router>
            <div className="App">
                <DefaultLayout>
                    <Routes>
                        {publicRoutes.map((route, index) => {
                            const RouteName = route.element;
                            return <Route key={index} path={route.path} element={<RouteName />} />;
                        })}
                    </Routes>
                </DefaultLayout>
            </div>
        </Router>
    );
}

export default App;
