const images = {
    logo: require('./logo/logo.png'),
    avata: require('./avatas/avata.jpeg'),
    downloadImage: require('./logo/download.png'),
    searchIcon: require('./icons/search.svg').default,
    plusIcon: require('./icons/plus.svg').default,
    downloadIcon: require('./icons/download.svg').default,
    messageIcon: require('./icons/message.svg').default,
    notiIcon: require('./icons/noti.svg').default,
};
// Header Images

export default images;
