const tags = [
    {
        id: 1,
        title: 'Giới thiệu',
        link: '/about',
    },
    {
        id: 2,
        title: 'Bảng tin',
        link: '/news',
    },
    {
        id: 3,
        title: 'Liên hệ',
        link: '/contact',
    },
    {
        id: 4,
        title: 'Sự nghiệp',
        link: '/jobs',
    },
    {
        id: 5,
        title: 'Tiktok for Good',
        link: '/tiktok-for-good',
    },
    {
        id: 6,
        title: 'Quảng cáo',
        link: '/advertisement',
    },
    {
        id: 7,
        title: 'Developer',
        link: '/developer',
    },
    {
        id: 8,
        title: 'Minh bạch',
        link: '/transparency',
    },
    {
        id: 9,
        title: 'Tiktok Reward',
        link: '/tiktok-reward',
    },
    {
        id: 10,
        title: 'Tiktok Embes',
        link: '/tiktok-embes',
    },
    {
        id: 11,
        title: 'Trợ giúp an toàn',
        link: '/help-to-safe',
    },
    {
        id: 12,
        title: 'Quyền riêng tư',
        link: '/privacy-policy',
    },
    {
        id: 13,
        title: 'Cổng thông tin tác giả',
        link: '/creator-info',
    },
    {
        id: 14,
        title: 'Hướng dẫn cộng đồng',
        link: 'comunity-guidelines',
    },
];

export default tags;
