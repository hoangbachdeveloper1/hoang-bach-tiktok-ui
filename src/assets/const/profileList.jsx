export const profileList = [
    {
        title: 'Xem hồ sơ',
        link: '/profile',
        icon: <i className="far fa-user-alt"></i>,
    },
    {
        title: 'Yêu thích',
        link: '/list-love',
        icon: <i className="fal fa-heart"></i>,
    },
    {
        title: 'Nhận xu',
        link: '/coin',
        icon: <i className="fal fa-coins"></i>,
    },
    {
        title: 'Trung tâm nhà sáng tạo Live',
        link: '/create-live-center',
        icon: <i className="fal fa-lightbulb-on"></i>,
    },
    {
        title: 'Cài đặt',
        link: '/setting',
        icon: <i className="fal fa-cog"></i>,
    },
    {
        title: 'Ngôn ngữ',
        link: '/language',
        icon: <i className="fal fa-language"></i>,
    },
    {
        title: 'Phản hồi và trợ giúp',
        link: '/feedback-and-help',
        icon: <i className="fal fa-question-circle"></i>,
    },
    {
        title: 'Phím tắt trên bàn phím',
        link: 'keyboard-shortcuts',
        icon: <i className="fal fa-keyboard"></i>,
    },
    {
        title: 'Chế độ tối',
        link: '',
        mode: true,
        icon: <i className="fal fa-moon"></i>,
    },
    {
        title: 'Đăng xuất',
        link: '/logout',
        icon: <i className="fal fa-sign-out-alt"></i>,
        logOut: true,
    },
];
