import styles from './Home.module.scss';
import classNames from 'classnames/bind';
import VideoComponent from '@/components/VideoComponent/VideoComponent';

const cx = classNames.bind(styles);

function Home() {
    return (
        <div id="home">
            <div className={cx('home')}>
                <VideoComponent />
                <VideoComponent />
                <VideoComponent />
                <VideoComponent />
                <VideoComponent />
                <VideoComponent />
                <VideoComponent />
            </div>
        </div>
    );
}

export default Home;
