import { Link } from 'react-router-dom';
import styles from './VideoComponent.module.scss';
import classNames from 'classnames/bind';
import videos from '@/assets/videos';
import { useState } from 'react';
const cx = classNames.bind(styles);

function VideoComponent() {
    const [visible, setVisible] = useState(false);
    const [follow, setFollow] = useState(false);

    return (
        <div className={cx('default__item', 'item')}>
            <div className={cx('item__container')}>
                {/* Avata Start */}
                <div className={cx('item__avata')}>
                    <Link to={null}>
                        <img
                            src="https://p9-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/1b82c01d05bd860c1fc117ccf953e981.jpeg?x-expires=1693908000&x-signature=YrOyxmrfHsMJkSfpK8QavsmsOkY%3D"
                            alt="avata"
                        />
                    </Link>
                </div>
                {/* Avata End */}

                {/* Content start */}
                <div className={cx('item__content')}>
                    {/* Header Start */}
                    <div className={cx('item__header')}>
                        {/* Info Start */}
                        <div className={cx('item__info')}>
                            {/* Profile Start */}
                            <div className={cx('item__info__profile')}>
                                <div className={cx('item__info__slug')}>
                                    <Link to={null}>dang_dinh_trung</Link>
                                </div>
                                <div className={cx('item__info__name')}>
                                    <Link to={null}>TTAV shop</Link>
                                </div>
                            </div>
                            {/* Profile End */}

                            {/* Description Start */}
                            <div className={cx('item__info__description')}>
                                <span>Trả lời</span> &nbsp;
                                <Link to={null}>
                                    <strong>@</strong>
                                    <strong>Khải Nguyễn</strong>
                                </Link>
                                &nbsp;
                                <span>Bảng xếp hạng Oruku</span>
                                <Link to={null}>#siêunhângao</Link>
                                <Link to={null}>#Master2023byTikTok</Link>
                                <Link to={null}>#theanh28</Link>
                                <Link to={null}>#reviewphimhay</Link>
                                <Link to={null}>#Gao</Link>
                                <Link to={null}>#reviewphim</Link>
                            </div>
                            <button className={cx('item__info__more')}>thêm</button>
                            {/* Description End */}

                            {/* Music Start */}
                            <div className={cx('item__info__music')}>
                                <h4>
                                    <Link>
                                        <i class="fal fa-music"></i>
                                        <span>Nhạc nền - </span>
                                        <span>MEXINE</span>
                                    </Link>
                                </h4>
                            </div>
                            {/* Music End */}
                        </div>
                        {/* Info End */}
                        {/* Follow Button Start */}
                        <div className={cx('item__button')}>
                            <button className={cx(follow ? 'follow' : '')} onClick={() => setFollow(!follow)}>
                                {follow ? 'Đang Follow' : 'Follow'}
                            </button>
                        </div>

                        {/* Follow Button End */}
                    </div>
                    {/* Header End */}

                    {/* Video Container Start */}
                    <div className={cx('item__video')}>
                        <div className={cx('item__video__wraps')}>
                            {/* Video content Start */}
                            <div className={cx('item__video__content')}>
                                <div
                                    className={cx('item__video__ellipsis')}
                                    onMouseOver={() => {
                                        setVisible(true);
                                    }}
                                    onMouseOut={() => {
                                        setVisible(false);
                                    }}
                                >
                                    <div className={cx('ellipsis__icon')}>
                                        <i class="far fa-ellipsis-v"></i>
                                    </div>
                                    {/* Video Options Start */}
                                    <div
                                        className={cx(
                                            'item__video__option',
                                            visible ? 'item__video__option--visible' : '',
                                        )}
                                    >
                                        <ul className={cx('option__list')}>
                                            <li className={cx('option__item')}>
                                                <button>
                                                    <i class="far fa-heart-broken"></i>
                                                    <span>Không quan tâm</span>
                                                </button>
                                            </li>
                                            <li className={cx('option__item')}>
                                                <button>
                                                    <i class="fal fa-flag"></i>
                                                    <span>Báo cáo</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                    {/* Video Options End */}
                                </div>
                                <video controls>
                                    <source src={videos.video1} type="video/mp4" />
                                </video>

                                <div className={cx('item_video__controler')}>
                                    {/* seeker */}
                                    {/* Pause */}
                                    {/* volume */}
                                </div>
                            </div>
                            {/* Video content End */}

                            {/* Video Actions Start */}
                            <div className={cx('item__video__actions')}>
                                <div className={cx('actions__wraps')}>
                                    {/* Heart Start */}
                                    <div className={cx('actions__item', 'actions__item--heart')}>
                                        <button className={cx('actions__item__button')}>
                                            <span>
                                                <i class="fas fa-heart"></i>
                                            </span>
                                        </button>
                                        <h6 className={cx('actions__item__number')}>2351</h6>
                                    </div>
                                    {/* Heart End */}

                                    {/* Comment Start */}
                                    <div className={cx('actions__item', 'actions__item--comment')}>
                                        <button className={cx('actions__item__button')}>
                                            <span>
                                                <i class="fas fa-comment-dots"></i>
                                            </span>
                                        </button>
                                        <h6 className={cx('actions__item__number')}>2351</h6>
                                    </div>
                                    {/* Comment End */}

                                    {/* Like Start */}
                                    <div className={cx('actions__item', 'actions__item--like')}>
                                        <button className={cx('actions__item__button')}>
                                            <span>
                                                <i class="fas fa-thumbs-up"></i>
                                            </span>
                                        </button>
                                        <h6 className={cx('actions__item__number')}>2351</h6>
                                    </div>
                                    {/* Comment End */}

                                    {/* Share Start */}
                                    <div className={cx('actions__item', 'actions__item--share')}>
                                        <button className={cx('actions__item__button')}>
                                            <span>
                                                <i class="fas fa-share"></i>
                                            </span>
                                        </button>
                                        <h6 className={cx('actions__item__number')}>2351</h6>
                                    </div>
                                    {/* Share End */}
                                </div>
                            </div>
                            {/* Video Actions End */}
                        </div>
                    </div>
                    {/* Video Container End */}
                </div>
                {/* Content End */}
            </div>
        </div>
    );
}

export default VideoComponent;
