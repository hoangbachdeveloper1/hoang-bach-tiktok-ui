import styles from './Proper.module.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

function Proper({ children }) {
    return (
        <div className={cx('proper')} tabIndex="-1">
            <div className={cx('proper__container')}>{children}</div>
        </div>
    );
}

export default Proper;
