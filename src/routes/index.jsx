import Home from '@/pages/Home';
import Follow from '@/pages/Follow';
import New from '@/pages/New';
import Live from '@/pages/Live';

export const publicRoutes = [
    {
        path: '/',
        element: Home,
        name: 'Dành cho bạn',
    },
    {
        path: '/follow',
        element: Follow,
        name: 'Đang follow',
    },
    {
        path: '/new',
        element: New,
        name: 'Khám phá',
    },
    {
        path: '/live',
        element: Live,
        name: 'Trực tiếp',
    },
];
